import ModuleContext from "@/context/ModuleContext";

import ModuleHeader from '@/components/ModuleHeader/ModuleHeader';
import ModuleContent from '@/components/ModuleContent/ModuleContent';

function Module2_Page(){

    return (
        <ModuleContext.Provider value={{
            activeModule: {
                name: 'MOD 2'
            }
        }}>
            <div>
                <ModuleHeader />
                <ModuleContent />
            </div>
        </ModuleContext.Provider>
    )
}

export default Module2_Page;