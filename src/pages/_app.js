import 'antd/dist/antd.min.css';
import 'antd/dist/antd.dark.min.css';

import '../styles/globals.css'


import AppContextProvider from '../context/AppContextProvider';

function _App ({ Component, pageProps }) {

  return (
  
    <AppContextProvider>
      
      <Component {...pageProps} />

    </AppContextProvider>

  )
}

export default _App
