import ModuleContext from "@/context/ModuleContext";

import ModuleHeader from '@/components/ModuleHeader/ModuleHeader';
import ModuleContent from '@/components/ModuleContent/ModuleContent';

function Module1_Page(){

    return (
        <ModuleContext.Provider value={{
            activeModule: {
                name: 'MOD 1'
            }
        }}>
            <div>
                <ModuleHeader />
                <ModuleContent />
            </div>
        </ModuleContext.Provider>
    )
}

export default Module1_Page;