
import Header from "@/components/Header/Header";
import Footer from "@/components/Footer/Footer";
import Content from "@/components/Content/Content";

export default function Home() {

  return (
    <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
      <Header />
      <Content />
      <Footer />
    </div>
  )
}
