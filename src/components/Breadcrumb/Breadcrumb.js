import { useContext } from "react";
import ModuleContext from "@/context/ModuleContext"

const Breadcrumb = () => {

    const moduleContext = useContext(ModuleContext);

    return (
        <div>
            BREADCRUMB {moduleContext.activeModule.name}
        </div>
    )
}

export default Breadcrumb;