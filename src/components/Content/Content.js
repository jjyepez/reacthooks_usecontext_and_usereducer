import { 
    List,
    Card
 } from "antd";

import styles from './Content.module.css';

const Content = () => {

    const data = [1,2,3,4];

    return (
        <div className={styles.Content}>
            
            <List
                style={{flex: .5}}
                size='small'
                header={<div>Header</div>}
                footer={<div>Footer</div>}
                bordered
                dataSource={data}
                rowKey={item=>item}
                renderItem={item => (
                    <List.Item onClick={(ev)=>console.log(ev.target.value)}>
                        {item}
                    </List.Item>
                )}
            />

            <Card>
                X
            </Card>

        </div>
    )
}

export default Content;