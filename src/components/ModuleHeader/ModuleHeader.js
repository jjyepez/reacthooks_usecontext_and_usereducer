import Breadcrumb from '@/components/Breadcrumb/Breadcrumb'; 
import Header from '@/components/Header/Header';

const ModuleHeader = () => {
    return (
        <>
            <Header />
            <Breadcrumb />
        </>
    )
}

export default ModuleHeader;