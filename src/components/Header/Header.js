import {useContext, useEffect, useState} from 'react';

import Link from 'next/link';
import {
    Space,
    Button
} from 'antd';

import AppContext from "@/context/AppContext";

const Header = () => {
    
    const appContext = useContext(AppContext);

    const [activeUserChanged, setActiveUserChanged] = useState(null);
    const [activeUser, setActiveUser] = useState(appContext.session.activeUser);

    const loginHandle = () => {
        appContext.session.activeUser = 'JJY';
        setActiveUserChanged(Math.random());
    }

    const logoutHandle = () => {
        appContext.session.activeUser = null;
        setActiveUserChanged(Math.random());
    }

    useEffect(()=>{
        setActiveUser(appContext.session.activeUser);
    }, [activeUserChanged])

    return (
        <header>
            <div style={{display:'flex', alignItems:'center'}}>
                <div style={{flex:1}}>
                    HEADER {appContext.app.theme}
                </div>  

                <Space>
                    USER {activeUser}
                    <Button onClick={loginHandle}>Login</Button>
                    <Button onClick={logoutHandle}>Logout</Button>
                </Space>
            </div>
            
            <hr />

            <Space>
                <Link href='/' passHref>Home</Link>
                <Link href='/module_1' passHref>Module 1</Link>
                <Link href='/module_2' passHref>Module 2</Link>
            </Space>
            <hr />
        </header>
    )
}

export default Header;
