import { useContext, useState } from 'react';

import AppContext from "@/context/AppContext";
import ModuleContext from "@/context/ModuleContext";

const ModuleContent = () => {

    const appContext = useContext(AppContext);
    const moduleContext = useContext(ModuleContext);

    const [activeUser, setActiveUser] = useState(appContext.session.activeUser);

    setInterval( () => {
        setActiveUser(appContext.session.activeUser);
    }, 2000)

    return (
        <div>
            MODULE CONTENT for {moduleContext.activeModule.name}
            <br />
            Usuario activo en app: {activeUser}
        </div>
    )
}

export default ModuleContent;