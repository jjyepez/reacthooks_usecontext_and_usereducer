import { useContext } from 'react';
import AppContext from '../../context/AppContext';

const Footer = () => {

    const appContext = useContext(AppContext);
    
    return (
        <div>
            FOOTER {appContext.app.theme}
        </div>
    )

}

export default Footer;