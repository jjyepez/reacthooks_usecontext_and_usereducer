import { createContext } from 'react';

// Es el contenedor del estado, donde se van a almacenar
// y compartir las variables en toda la app
// similar a LocalStorage

const ModuleContext = createContext(null);

export default ModuleContext;
