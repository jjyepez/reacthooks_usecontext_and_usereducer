
import AppContext from './AppContext';
import AppState from '../config/AppState';

// Se designe el AppContext como wrapper de toda la aplicación (en este caso)
// y se le asigna el valor de estado inicial cargado desde el archivo de configuración

function AppContextProvider({ children }) {
    
    return (
        <AppContext.Provider value={AppState}>

            {children}
            
        </AppContext.Provider>
    )

}

export default AppContextProvider;
